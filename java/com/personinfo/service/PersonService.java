package com.personinfo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.personinfo.model.Person;
import com.personinfo.repository.PersonRepo;

@Service
public class PersonService {
	
	@Autowired
	PersonRepo personRepo;

	public int savePerson(Person person) {
		personRepo.save(person);
		return person.getPersonid();
		
	}

	public List<Person> getPersonData() {
		List<Person> persons = new ArrayList<Person>();
		personRepo.findAll().forEach(person -> persons.add(person));
		return persons;		
	}

	public void deletePerson(int personid) {
		// TODO Auto-generated method stub
		try {
		personRepo.deleteById(personid);
		}catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}

	public void editPerson(Person person) {
		// TODO Auto-generated method stub
		personRepo.save(person);		
	}

	public long personCount() {
		// TODO Auto-generated method stub
		long count=personRepo.count();
		return count;
	}
	

}
